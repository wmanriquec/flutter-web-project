class User {
  int id;
  String name;
  String email;

  User({
    this.id,
    this.name,
    this.email,
  });

  //@override
  Map<String, dynamic> asMap() {
    return {
      "id"    : id,
      "name"  : name,
      "email" : email
    };
  }
  factory User.fromMap(Map<String, dynamic> json) => new User(
    id: json["id"] as int,
    name: json['name'] as String,
    email: json['email'] as String,
  );
  // ignore: avoid_renaming_method_parameters
}
