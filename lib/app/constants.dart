import 'package:flutter/material.dart';

Color kPrimaryColor = Color(0xFF50B158);

enum Option {
  LogIn, SignUp
}