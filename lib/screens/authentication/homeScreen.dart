import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:iw_project/app/constants.dart';
import 'package:iw_project/screens/authentication/productDeatil.dart';
import 'package:iw_project/screens/authentication/productEdit.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomeScreen> {
  List _data;
  Future products() async{
    var url = 'http://localhost:8888/products'; 
    Map<String, String> userHeader = {'Content-type': 'application/json',};

    var response = await http.get(url, headers:userHeader);
    print(response.body.toString());
    print("asda");
    if (response.statusCode==200){
      final jsonData = json.decode(response.body);
      print(jsonData);
      print("aaaaaaaaaa");
      if (jsonData['state']=='nice') {
        print(jsonData['values']);
        /*setState(() {
          _data=jsonData['values'];
        });*/
          _data=jsonData['values'];
        return jsonData['values'];
      } else {
        return null;
      }
    }
    else{
      print("bbbbbbbbbb");
      return null;
    }
  }


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: kPrimaryColor,
        body: FutureBuilder(
          future: products(),
          builder: (context, projectSnap) {
            if (projectSnap.hasError) {
              return Text(
                'There was an error :(',
                style: Theme.of(context).textTheme.headline,
              );
            } 
            else if (projectSnap.hasData) {
              return ListView.builder(
                padding: const EdgeInsets.all(5.5),
                itemCount: _data.length,
                itemBuilder: _itemBuilder,
              );
            } 
            else {
              return Center(
                child:CircularProgressIndicator(),
              );
            }
            //return Container();
            
          },
        ), 
        floatingActionButton: FloatingActionButton(onPressed: () {
          Map products ={
            'id':0,
            'name':'',
            'description':'',
            'price':0
          };
          Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => ProductEdit(
              product:products
            )));
        },
          backgroundColor: Colors.orange,
          child: Icon(Icons.add),
        ),
      
      ),
    );
  }

  Widget _itemBuilder(BuildContext context, int index) {
    return InkWell(
      onTap: (){
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => ProductDetail(
            product:_data[index]
          )));
      },
      child: Card(
        child: Center(
          child:ListTile(
            title: Text("Nombre: ${_data[index]['name']}"),
            subtitle: Column(
              children:[
                Text("Descripción: ${_data[index]['description']}"),
                Text(
                  "Precio: ${_data[index]['price']}",
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Colors.orange,
                  ),
                ),
              ]
            ),
          ),
        ),
      ),
      /*onTap: () => MaterialPageRoute(
        builder: (context) =>
            SecondRoute(id: _data.getId(index), name: _data.getName(index))),*/
    );
  }
}