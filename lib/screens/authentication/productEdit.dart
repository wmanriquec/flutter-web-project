
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:iw_project/app/constants.dart';
import 'package:http/http.dart' as http;


class ProductEdit extends StatelessWidget {
  final product;
  var nameController= TextEditingController();
  var descriptionController= TextEditingController();
  var priceController= TextEditingController();

  Future<bool> update() async{
    var url = 'http://localhost:8888/products/${product['id']}'; 
    Map<String, String> userHeader = {'Content-type': 'application/json',};
    final bodyRequest= json.encode( {"name":nameController.text,"description":descriptionController.text,"price":priceController.text});

    var response = await http.put(url, headers:userHeader, body:bodyRequest);
    print(response.body.toString());
    if (response.statusCode==200){
      final jsonData = json.decode(response.body);
      if (jsonData['state']=='nice') {
        return true;
      } else {
        return null;
      }
    }
    else{
      return null;
    }
  }

  Future<bool> create() async{
    var url = 'http://localhost:8888/products'; 
    Map<String, String> userHeader = {'Content-type': 'application/json',};
    final bodyRequest= json.encode( {"name":nameController.text,"description":descriptionController.text,"price":priceController.text});

    var response = await http.post(url, headers:userHeader, body:bodyRequest);
    print(response.body.toString());
    if (response.statusCode==200){
      final jsonData = json.decode(response.body);
      if (jsonData['state']=='nice') {
        return true;
      } else {
        return null;
      }
    }
    else{
      return null;
    }
  }

  ProductEdit({this.product});
  @override
  Widget build(BuildContext context) {
    
        return Scaffold(
    
    
          body: ListView(
            children: [
              SizedBox(height: 15.0),
              Padding(
                padding: EdgeInsets.only(left: 20.0),
                child: Text(
                  'Producto',
                  style: TextStyle(
                    fontSize: 42.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  )
                ),
              ),
                /*SizedBox(height: 15.0),
                Hero(
                  tag: assetPath,
                  child: Image.asset(assetPath,
                  height: 150.0,
                  width: 100.0,
                  fit: BoxFit.contain
                  )
                ),*/
              SizedBox(height: 20.0),
              TextField(
                controller: nameController..text=product['name'],
                  decoration: InputDecoration(
                  hintText: 'Ingrese nombre de producto',
                  labelText: 'Nombre',
                  suffixIcon: Icon(
                    Icons.panorama_photosphere
                  ),
                ),
              ),
              SizedBox(height: 20.0),
              TextField(
                controller: descriptionController..text=product['description'],
                  decoration: InputDecoration(
                  hintText: 'Ingrese descripción de producto',
                  labelText: 'Descripción',
                  suffixIcon: Icon(
                    Icons.pending,
                  ),
                ),
              ),
              SizedBox(height: 20.0),
              TextField(
                controller: priceController..text=product['price'].toString(),
                  decoration: InputDecoration(
                  hintText: 'Ingrese precio de producto',
                  labelText: 'Precio',
                  suffixIcon: Icon(
                    Icons.monetization_on,
                  ),
                ),
              ),
            
            SizedBox(height: 20.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ButtonTheme(
                  //minWidth: double.infinity,
                  height: 50.0,
                  child:RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: kPrimaryColor.withOpacity(0.2),)),
                    onPressed: () async {
                      if(product['id']!=0){
                        bool updated=await update();
                        if (updated==true){
                          Navigator.popAndPushNamed(
                            context,
                            '/dashboard',
                          );
                        
                        }
                        else{
                        }
                      }
                      else{
                        bool insert=await create();
                        if (insert==true){
                          Navigator.popAndPushNamed(
                            context,
                            '/dashboard',
                          );
                        }
                        else{
                        }
                      }
                    },
                    color: kPrimaryColor,
                    textColor: Colors.white,
                    child: Text("Guardar",
                      style: TextStyle(fontSize: 14)),
                  ),
                ),
                SizedBox(width: 20.0),
                ButtonTheme(
                  //minWidth: double.infinity,
                  height: 50.0,
                  child:RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: kPrimaryColor.withOpacity(0.2),)),
                    onPressed: () {
                      Navigator.pop(
                        context,
                        '/dashboard',
                      );
                    },
                    color: kPrimaryColor,
                    textColor: Colors.white,
                    child: Text("Cancelar",
                      style: TextStyle(fontSize: 14)),
                  ),
                ),
              ],
            )
        ]
      ),
    );
  }
}