
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:iw_project/app/constants.dart';
import 'package:iw_project/screens/authentication/productEdit.dart';
import 'package:http/http.dart' as http;


class ProductDetail extends StatelessWidget {
  final product;

  Future<bool> delete() async{
    var url = 'http://localhost:8888/products/${product['id']}'; 
    Map<String, String> userHeader = {'Content-type': 'application/json',};
    //final bodyRequest= json.encode( {"name":nameController.text,"description":descriptionController.text,"price":priceController.text});

    var response = await http.delete(url, headers:userHeader);
    print(response.body.toString());
    if (response.statusCode==200){
      final jsonData = json.decode(response.body);
      if (jsonData['state']=='nice') {
        return true;
      } else {
        return null;
      }
    }
    else{
      return null;
    }
  }

  ProductDetail({this.product});
  @override
  Widget build(BuildContext context) {
    return Scaffold(


      body: ListView(
        children: [
          SizedBox(height: 15.0),
          Padding(
            padding: EdgeInsets.only(left: 20.0),
            child: Text(
              'Producto',
              style: TextStyle(
                fontSize: 42.0,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              )
            ),
          ),
            /*SizedBox(height: 15.0),
            Hero(
              tag: assetPath,
              child: Image.asset(assetPath,
              height: 150.0,
              width: 100.0,
              fit: BoxFit.contain
              )
            ),*/
            SizedBox(height: 20.0),
            Center(
              child: Text(product['name'],
                  style: TextStyle(
                      fontSize: 22.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black
                  )
              ),
            ),
            SizedBox(height: 10.0),
            Center(
              child: Text(product['description'],
                  style: TextStyle(
                      color: Color(0xFF575E67),
                      fontSize: 24.0)),
            ),
            SizedBox(height: 20.0),
            Center(
              child: Container(
                width: MediaQuery.of(context).size.width - 50.0,
                child: Text("Precio: ${product['description']}",
                textAlign: TextAlign.center,
                style: TextStyle(
                      fontFamily: 'Varela',
                      fontSize: 16.0,
                      color: Colors.orange)
                ),
              ),
            ),
            SizedBox(height: 20.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ButtonTheme(
                  //minWidth: double.infinity,
                  height: 50.0,
                  child:RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: kPrimaryColor.withOpacity(0.2),)),
                    onPressed: ()  {
                      Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => ProductEdit(
                          product:product
                        )));
                    },
                    color: kPrimaryColor,
                    textColor: Colors.white,
                    child: Text("Editar",
                      style: TextStyle(fontSize: 14)),
                  ),
                ),
                SizedBox(width: 20.0),
                ButtonTheme(
                  //minWidth: double.infinity,
                  height: 50.0,
                  child:RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: kPrimaryColor.withOpacity(0.2),)),
                    onPressed: () async {
                      bool deleted=await delete();
                      if (deleted==true){
                        Navigator.popAndPushNamed(
                          context,
                          '/dashboard',
                        );
                      }
                      else{
                      }
                    },
                    color: kPrimaryColor,
                    textColor: Colors.white,
                    child: Text("Eliminar",
                      style: TextStyle(fontSize: 14)),
                  ),
                ),
              ],
            )
        ]
      ),
    );
  }
}