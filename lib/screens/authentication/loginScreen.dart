import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:iw_project/app/constants.dart';
import 'package:iw_project/database/users/userDB.dart';
import 'package:iw_project/models/user.dart';
import 'package:iw_project/objects/action_button.dart';
import 'package:iw_project/provider/appProvider.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

class LogIn extends StatefulWidget {

  final Function onSignUpSelected;

  LogIn({@required this.onSignUpSelected});

  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  final emailController= TextEditingController();
  final passwordController= TextEditingController();
  //UserDataBase userDb;

  String menssage="";



  Future<User> login() async{
    var url = 'http://localhost:8888/user/login'; 
    Map<String, String> userHeader = {'Content-type': 'application/json',};
    final bodyRequest= json.encode( {"email":emailController.text,"password":passwordController.text});

    var response = await http.post(url, headers:userHeader, body:bodyRequest);
    print(response.body.toString());
    if (response.statusCode==200){
      final jsonData = json.decode(response.body);
      if (jsonData['state']=='nice') {
        User temp= User.fromMap(jsonData['values']);
        return temp;
      } else {
        return null;
      }
    }
    else{
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;
    
    
        return Padding(
          padding: EdgeInsets.all(size.height > 770 ? 64 : size.height > 670 ? 32 : 16),
          child: Center(
            child: Card(
              elevation: 4,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(25),
                ),
              ),
              child: AnimatedContainer(
                duration: Duration(milliseconds: 200),
                height: size.height * (size.height > 770 ? 0.7 : size.height > 670 ? 0.8 : 0.9),
                width: 500,
                color: Colors.white,
                child: Center(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.all(40),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
    
                          Text(
                            "Iniciar Sesión",
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.grey[700],
                            ),
                          ),
    
                          SizedBox(
                            height: 8,
                          ),
    
                          Container(
                            width: 30,
                            child: Divider(
                              color: kPrimaryColor,
                              thickness: 2,
                            ),
                          ),
    
                          SizedBox(
                            height: 32,
                          ),
    
                          TextField(
                            controller: emailController,
                            decoration: InputDecoration(
                              hintText: 'Ingrese su correo',
                              labelText: 'Correo',
                              suffixIcon: Icon(
                                Icons.mail_outline,
                              ),
                            ),
                          ),
    
                          SizedBox(
                            height: 32,
                          ),
    
                          TextField(
                            controller: passwordController,
                            decoration: InputDecoration(
                              hintText: 'Ingrese su contraseña',
                              labelText: 'Contraseña',
                              suffixIcon: Icon(
                                Icons.lock_outline,
                              ),
                            ),
                          ),
    
                          SizedBox(
                            height: 44,
                          ),

                          Text(
                            menssage,
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.red,
                              ),
                            ),
                          //actionButton("Log In"),
                          ButtonTheme(
                            minWidth: double.infinity,
                            height: 50.0,
                            child:RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                                side: BorderSide(color: kPrimaryColor.withOpacity(0.2),)),
                              onPressed: () async {
                                
                                User temp=await login();
                                if (temp!=null){
                                  Provider.of<AppProvider>(context, listen: false)
                                  .setUser(temp);
                                  Navigator.pushReplacementNamed(
                                    context,
                                    '/dashboard',
                                  );
                                }
                                else{
                                  setState(() {
                                    menssage="Datos incorrectos";
                                  });
                                }
                              },
                              color: kPrimaryColor,
                              textColor: Colors.white,
                              child: Text("Iniciar Sesión",
                                style: TextStyle(fontSize: 14)),
                            ),
                          ),
    
                          SizedBox(
                            height: 32,
                          ),
    
                          

                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [

                          Text(
                            "Si no tiene cuenta,",
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 14,
                            ),
                          ),

                          SizedBox(
                            width: 8,
                          ),

                          GestureDetector(
                            onTap: () {
                              widget.onSignUpSelected();
                            },
                            child: Row(
                              children: [

                                Text(
                                  "Registrese",
                                  style: TextStyle(
                                    color: kPrimaryColor,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),

                                SizedBox(
                                  width: 8,
                                ),

                                Icon(
                                  Icons.arrow_forward,
                                  color: kPrimaryColor,
                                ),
                                
                              ],
                            ),
                          ),

                        ],
                      ),

                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}