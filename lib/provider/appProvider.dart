import 'package:flutter/cupertino.dart';
import 'package:iw_project/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppProvider extends ChangeNotifier {
  User _user;


  get user => _user;


  void setUser(value) {
    _user = value;
    notifyListeners();
  }

}